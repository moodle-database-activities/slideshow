# Slideshow

Slideshow is a preset for the Moodle activity database.

## Demo

https://fdagner.de/moodle/mod/data/view.php?d=6&rid=24

## Getting started

Download the source code and zip the files WITHOUT music, screenshots and parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import". A repository must be created for the music in Moodle. The link to the directory must be customized in the single view template.

## Language Support

The preset is available in German. 

## Description

The slideshow allows students to create timed slides and add text, photo and music.

## License

CC BY-NC-SA 2.0 DE

## Screenshots

<img width="400" alt="single view" src="/screenshots/einzelansicht.png">

<img width="400" alt="list view" src="/screenshots/listenansicht.png">
